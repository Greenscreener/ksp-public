package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	var N int

	fmt.Fscanf(in, "%d\n", &N)
	balances := make(map[string]int)
	for n := 0; n < N; n++ {
		var C, P int
		var platic string
		fmt.Fscanf(in, "%d %s %d ", &C, &platic, &P)
		c := C / P
		if _, ok := balances[platic]; !ok {
			balances[platic] = 0
		}
		balances[platic] += C
		for p := 0; p < P; p++ {
			var dluznik string
			fmt.Fscanf(in, "%s", &dluznik)
			if _, ok := balances[dluznik]; !ok {
				balances[dluznik] = 0
			}
			balances[dluznik] -= c
		}
		fmt.Fscanf(in, "\n")
	}
	for i, val := range balances {
		fmt.Printf("%s %d\n", i, val)
	}

}
