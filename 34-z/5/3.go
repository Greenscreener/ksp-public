package main

import (
	"bufio"
	"fmt"
	"os"
)

type queue struct {
	start *indice
	end   *indice
}

type indice struct {
	value int
	next  *indice
}

func newQueue() queue {
	return queue{nil, nil}
}

func (q *queue) enqueue(value int) {
	i := indice{value, nil}
	if q.start == nil {
		q.start = &i
		q.end = &i
	} else {
		e := q.end
		q.end = &i
		e.next = &i
	}
}

func (q *queue) dequeue() int {
	s := q.start
	q.start = s.next
	return s.value
}

func (q *queue) isEmpty() bool {
	if q.start == nil {
		return true
	} else {
		return false
	}
}

func main() {
	in := bufio.NewReader(os.Stdin)

	var N, K int

	fmt.Fscanf(in, "%d %d\n", &N, &K)
	visited := make([]bool, N)
	neighbors := make([][]int, N)
	for n := 0; n < N; n++ {
		neighbors[n] = make([]int, 0)
	}

	for k := 0; k < K; k++ {
		var a, b int
		fmt.Fscanf(in, "%d %d\n", &a, &b)
		neighbors[a] = append(neighbors[a], b)
		neighbors[b] = append(neighbors[b], a)
	}

	for n := 0; n < N; n++ {
		// fmt.Fprintf(os.Stderr, "%d: %v\n", n, neighbors[n])
	}

	islands := 0

	for n := 0; n < N; n++ {
		if visited[n] {
			continue
		}
		// fmt.Fprintf(os.Stderr, "BFSing from %d\n", n)
		islands++
		q := newQueue()
		q.enqueue(n)
		for !q.isEmpty() {
			c := q.dequeue()
			// fmt.Fprintf(os.Stderr, "Visiting %d\n", c)
			visited[c] = true
			for _, v := range neighbors[c] {
				if !visited[v] {
					// fmt.Fprintf(os.Stderr, "Enqueuing %d\n", v)
					q.enqueue(v)
				}
			}
		}
	}

	fmt.Println(islands - 1)

}
