package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	var N int

	fmt.Fscanf(in, "%d\n", &N)

	for n := 0; n < N; n++ {
		var s string
		fmt.Fscanf(in, "%s\n", &s)
		f := 1
		o := 0
		for _, c := range s {
			switch c {
			case 'R':
				o = (o + f + 4) % 4
			case 'L':
				o = (o - f + 4) % 4
			case 'H':
				f = -f
				o = (o + 2) % 4
			case 'V':
				f = -f
			}
		}
		if f == 1 && o == 0 {
			fmt.Println("b")
		} else if f == 1 && o == 2 {
			fmt.Println("q")
		} else if f == -1 && o == 0 {
			fmt.Println("d")
		} else if f == -1 && o == 2 {
			fmt.Println("p")
		} else {
			fmt.Println("?")
		}
	}
}
