package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type queue struct {
	start *indice
	end   *indice
}

type indice struct {
	value string
	next  *indice
}

func newQueue() queue {
	return queue{nil, nil}
}

func (q *queue) enqueue(value string) {
	i := indice{value, nil}
	if q.start == nil {
		q.start = &i
		q.end = &i
	} else {
		e := q.end
		q.end = &i
		e.next = &i
	}
}

func (q *queue) dequeue() string {
	s := q.start
	q.start = s.next
	return s.value
}

func (q *queue) isEmpty() bool {
	if q.start == nil {
		return true
	} else {
		return false
	}
}

type definition struct {
	a  string
	b  string
	op rune
}
type codefinition struct {
	a    string
	op   rune
	side bool
}

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func main() {
	in := bufio.NewReader(os.Stdin)

	var N, M int
	var X string

	fmt.Fscanf(in, "%d %d %s\n", &N, &M, &X)

	known := make(map[string]bool)
	weights := make(map[string]int)

	s, _ := in.ReadString('\n')
	s = s[:len(s)-1]
	knowns := strings.Split(s, " ")
	for _, k := range knowns {
		known[k] = true
		weights[k] = 0
	}
	oKnown := known
	fmt.Fprintln(os.Stderr, knowns)

	definitions := make(map[string][]definition)
	bestDefinitions := make(map[string]definition)
	definable := make(map[string][]string)
	codefinable := make(map[string]map[string]codefinition)
	useful := make(map[string]bool)

	for m := 0; m < M; m++ {
		var a, b, c string
		var op rune
		fmt.Fscanf(in, "%s = %s %c %s\n", &c, &a, &op, &b)
		definitions[c] = append(definitions[c], definition{a, b, op})
		definable[a] = append(definable[a], c)
		definable[b] = append(definable[b], c)
		if codefinable[c] == nil {
			codefinable[c] = make(map[string]codefinition)
		}
		codefinable[c][a] = codefinition{b, op, true}
		codefinable[c][b] = codefinition{a, op, false}

	}

	q := newQueue()
	q.enqueue(X)
	for !q.isEmpty() {
		c := q.dequeue()
		if useful[c] {
			continue
		}
		useful[c] = true
		if oKnown[c] {
			continue
		}
		for _, def := range definitions[c] {
			q.enqueue(def.a)
			q.enqueue(def.b)
		}
	}

	halfKnown := make(map[string]map[string]bool)
	xKnown := false
	for !xKnown {
		newKnown := make(map[string]bool)
		for a := range known {
			// fmt.Fprintf(os.Stderr, "Checking all that can be defined from %s.\n", a)
			for _, c := range definable[a] {
				// fmt.Fprintf(os.Stderr, "%s can be defined from %s.\n", c, a)
				b := codefinable[c][a].a
				op := codefinable[c][a].op
				side := codefinable[c][a].side
				if halfKnown[c][b] {
					newKnown[c] = true
					if c == X {
						xKnown = true
					}
					cW, cWExists := weights[c]
					if !cWExists || max(weights[a], weights[b])+1 < cW {
						weights[c] = max(weights[a], weights[b]) + 1
						if side {
							bestDefinitions[c] = definition{a, b, op}
							// fmt.Fprintf(os.Stderr, "The new best definition for %s is now %s %c %s.\n", c, a, op, b)
						} else {
							bestDefinitions[c] = definition{b, a, op}
							// fmt.Fprintf(os.Stderr, "The new best definition for %s is now %s %c %s.\n", c, b, op, a)
						}
					}
				} else if useful[c] {
					if halfKnown[c] == nil {
						halfKnown[c] = make(map[string]bool)
					}
					halfKnown[c][a] = true
					// fmt.Fprintf(os.Stderr, "%s could be defined from %s and something.\n", c, a)
				}
			}
		}
		known = newKnown
	}

	sortedCs := make([]string, 0)
	sortedAlready := oKnown

	appendAllChildrenThenSelf(X, bestDefinitions, &sortedCs, &sortedAlready)

	// for i := len(sortedCs) - 1; i >= 0; i-- {
	for _, c := range sortedCs {
		//	c := sortedCs[i]
		def := bestDefinitions[c]
		fmt.Printf("%s = %s %c %s\n", c, def.a, def.op, def.b)
	}

}
func appendAllChildrenThenSelf(c string, bestDefinitions map[string]definition, sortedCs *[]string, sortedAlready *map[string]bool) {
	if !(*sortedAlready)[c] {
		def := bestDefinitions[c]
		appendAllChildrenThenSelf(def.a, bestDefinitions, sortedCs, sortedAlready)
		appendAllChildrenThenSelf(def.b, bestDefinitions, sortedCs, sortedAlready)
		*sortedCs = append(*sortedCs, c)
		(*sortedAlready)[c] = true
	}
}
