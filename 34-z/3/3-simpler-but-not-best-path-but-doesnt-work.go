package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type queue struct {
	start *indice
	end   *indice
}

type indice struct {
	value string
	next  *indice
}

func newQueue() queue {
	return queue{nil, nil}
}

func (q *queue) enqueue(value string) {
	i := indice{value, nil}
	if q.start == nil {
		q.start = &i
		q.end = &i
	} else {
		e := q.end
		q.end = &i
		e.next = &i
	}
}

func (q *queue) dequeue() string {
	s := q.start
	q.start = s.next
	return s.value
}

func (q *queue) isEmpty() bool {
	if q.start == nil {
		return true
	} else {
		return false
	}
}

type definition struct {
	a  string
	b  string
	op rune
}
type codefinition struct {
	a    string
	op   rune
	side bool
}

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func main() {
	in := bufio.NewReader(os.Stdin)

	var N, M int
	var X string

	fmt.Fscanf(in, "%d %d %s\n", &N, &M, &X)

	known := make(map[string]bool)
	weights := make(map[string]int)

	s, _ := in.ReadString('\n')
	s = s[:len(s)-1]
	knowns := strings.Split(s, " ")
	for _, k := range knowns {
		known[k] = true
		weights[k] = 0
	}
	fmt.Fprintln(os.Stderr, knowns)

	definitions := make(map[string][]definition)

	for m := 0; m < M; m++ {
		var a, b, c string
		var op rune
		fmt.Fscanf(in, "%s = %s %c %s\n", &c, &a, &op, &b)
		definitions[c] = append(definitions[c], definition{a, b, op})
	}

	sortedCs := make([]string, 0)

	appendAllChildrenThenSelf(X, definitions, &sortedCs, &known)

	for _, c := range sortedCs {
		def := definitions[c][0]
		fmt.Printf("%s = %s %c %s\n", c, def.a, def.op, def.b)
	}

}
func appendAllChildrenThenSelf(c string, definitions map[string][]definition, sortedCs *[]string, sortedAlready *map[string]bool) {
	if !(*sortedAlready)[c] {
		for _, def := range definitions[c] {
			appendAllChildrenThenSelf(def.a, definitions, sortedCs, sortedAlready)
			appendAllChildrenThenSelf(def.b, definitions, sortedCs, sortedAlready)
		}
		*sortedCs = append(*sortedCs, c)
		(*sortedAlready)[c] = true
	}
}
