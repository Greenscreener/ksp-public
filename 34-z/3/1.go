package main

import (
	"bufio"
	"fmt"
	"os"
)

var morse = map[rune]string{
	'A': ".-", 'B': "-...", 'C': "-.-.",

	'D': "-..", 'E': ".", 'F': "..-.",

	'G': "--.", 'H': "....", 'I': "..",

	'J': ".---", 'K': "-.-", 'L': ".-..",

	'M': "--", 'N': "-.", 'O': "---",

	'P': ".--.", 'Q': "--.-", 'R': ".-.",

	'S': "...", 'T': "-", 'U': "..-",

	'V': "...-", 'W': ".--", 'X': "-..-",

	'Y': "-.--", 'Z': "--..", '1': ".----",

	'2': "..---", '3': "...--", '4': "....-",

	'5': ".....", '6': "-....", '7': "--...",

	'8': "---..", '9': "----.", '0': "-----",
}

func main() {
	in := bufio.NewReader(os.Stdin)

	var N int

	fmt.Fscanf(in, "%d\n", &N)

	for i := 0; i < N; i++ {
		message := ""
		var c rune
		for {
			fmt.Fscanf(in, "%c", &c)
			if c == '\n' {
				break
			}
			message += morse[c]
		}
		palindromic := true
		for j := 0; j < len(message)/2; j++ {
			if message[j] != message[len(message)-j-1] {
				palindromic = false
				break
			}
		}
		if palindromic {
			fmt.Println("ANO")
		} else {
			fmt.Println("NE")
		}
	}
}
