package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	var R, S, N, M int

	fmt.Fscanf(in, "%d %d %d %d\n", &R, &S, &N, &M)

	symbols := make([][]bool, N+M)

	for i := 0; i < N+M; i++ {
		symbols[i] = make([]bool, R*S)
		for r := 0; r < R; r++ {
			for s := 0; s < S; s++ {
				var c rune
				fmt.Fscanf(in, "%c", &c)
				if c == '.' {
					symbols[i][r*S+s] = false
				} else if c == 'x' {
					symbols[i][r*S+s] = true
				} else {
					panic("Bruh: " + string(c))
				}
			}
			fmt.Fscanf(in, "\n")
		}
	}

	stops := symbols[:N]
	signs := symbols[N:]

	for m := 0; m < M; m++ {
		similar := 0
		for n := 0; n < N; n++ {
			sim := true
			for i := 0; i < R*S; i++ {
				if signs[m][i] && !stops[n][i] {
					sim = false
					break
				}
			}
			if sim {
				similar++
			}
		}
		if similar == 1 {
			fmt.Println("ANO")
		} else {
			fmt.Println("NE")
		}
	}

}
