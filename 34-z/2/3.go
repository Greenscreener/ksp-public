package main

import (
	"fmt"
	   )

func main () {
	var N int
	
	fmt.Scanf("%d", &N)

	sizes := make([]int, N)
	maxBoxes := make([]int, N)

	overallMax := 0

	for i := 0; i < N; i++ {
		var A int

		fmt.Scanf("%d", &A)

		sizes[i] = A
		maxBoxes[i] = 1

		for j := 0; j < i; j++ {
			if sizes[j] < A && maxBoxes[j]+1 > maxBoxes[i] {
				maxBoxes[i] = maxBoxes[j]+1
			}
		}

		if maxBoxes[i] > overallMax {
			overallMax = maxBoxes[i]
		}

	}

	fmt.Println(overallMax)

}

