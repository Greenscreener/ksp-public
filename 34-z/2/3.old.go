package main

import (
	"fmt"
	"os"
	   )

type node struct {
	size int
	index int
	parents []*node
	children []*node
	maxDistance int
}

func newNode(size int, index int) node {
	parents := make([]*node, 0)
	children := make([]*node, 0)
	n := node{size, index, parents, children, 0}
	return n
}

func (n *node) String() string {
	return fmt.Sprintf("[%d,%d](%d)", n.size, n.index, n.maxDistance)
}

type queue struct {
	start *indice
	end *indice
}

type indice struct {
	value *node
	next *indice
}

func newQueue() queue {
	return queue{nil,nil}
}

func (q *queue) enqueue(value *node) {
	i := indice{value, nil}
	if q.start == nil {
		q.start = &i
		q.end = &i
	} else {
		e := q.end
		q.end = &i
		e.next = &i
	}	
}

func (q *queue) dequeue() *node {
	s := q.start
	q.start = s.next
	return s.value
}

func (q *queue) isEmpty() bool {
	if (q.start == nil) {
		return true
	} else {
		return false
	}
}


// Check if A fits into B
func fitsInto(b *node, a *node) bool {
	// fmt.Printf("Checking if [%d, %d] fits into [%d, %d].\n", a.size, a.index, b.size, b.index)
	// fmt.Println(a.index < b.index && a.size < b.size)
	return a.index < b.index && a.size < b.size
}

func findFirstFittingParent(old *node, added *node) *node {
	if fitsInto(added, old) {
		return old
	} else {
		q := newQueue()
		q.enqueue(old)
	    for !q.isEmpty() {
			o := q.dequeue()
			// fmt.Printf("BFS: Checking if parents of o%v fit into a%v\n", o, added)
			for i := range o.parents {
				// fmt.Printf("Checking %v\n", o.parents[i])
				if fitsInto(added, o.parents[i]) {
					return o.parents[i]
				} else {
					q.enqueue(o.parents[i])
				}
			}
		}
		return nil
	}
}

func exportDotFile(nodes []*node) {
	fmt.Println("digraph {")
	for i := range nodes {
		for j := range nodes[i].parents {
			fmt.Printf("\"%d-%d(%d)\" -> \"%d-%d(%d)\"\n", nodes[i].parents[j].size, nodes[i].parents[j].index, nodes[i].size, nodes[i].index)
		}
	}
	fmt.Println("}")
}

func main () {
	var N int
	
	fmt.Scanf("%d", &N)
	
	ends := make([]*node, 0)
	starts := make([]*node, 0)
		
	nodes := make([]*node, 0)

	fmt.Fprintf(os.Stderr, "\n")

	for i := 0; i < N; i++ {
		var D int
		fmt.Scanf("%d", &D)
		
		fmt.Fprintf(os.Stderr, "\r           \r%d/%d",i,N)

		n := newNode(D, i)

		nodes = append(nodes, &n)
		// fmt.Printf("Creating node %v.\n", n)
		
		// fmt.Printf("%v\n", ends)

		hasParents := false
		
		futureEnds := make([]*node, 0)

		for i := range ends {
			parent := findFirstFittingParent(ends[i], &n)
			if parent != nil {
				hasParents = true
				alreadyExists := false
				for i := range n.parents {
					if (n.parents[i] == parent) {
						alreadyExists = true
						break
					}
				} 
				if !alreadyExists {
					n.parents = append(n.parents, parent)
				}
			} 
			if parent != ends[i] {
				futureEnds = append(futureEnds, ends[i])
			}
		}

		if !hasParents {
			// fmt.Println("No parents.")
			starts = append(starts, &n)
			n.maxDistance = 1
		} else {
			n.maxDistance = 1
			for i := range n.parents {
				if n.maxDistance < n.parents[i].maxDistance+1 {
					n.maxDistance = n.parents[i].maxDistance+1
				}
			}
		}

		ends = append(futureEnds, &n)
	}

	fmt.Fprintf(os.Stderr, "\r                 \rDone!\n")
	// fmt.Printf("Starts: %+v\n",starts)
	// fmt.Printf("Ends: %+v\n\n",ends)


	overallMaxDistance := 0
	for i := range ends {
		if overallMaxDistance < ends[i].maxDistance {
			overallMaxDistance = ends[i].maxDistance
		}
	}

	fmt.Printf("%d\n", overallMaxDistance)
	//exportDotFile(nodes)

}


