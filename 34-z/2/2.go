package main

import (
	"fmt"
	   )

func main () {
	var N int
	
	fmt.Scanf("%d", &N)

	K := 0
	S := 0
	
	for i := 0; i < N; i++ {
		var A int

		fmt.Scanf("%d", &A)

		if K <= S {
			K+=A
		} else {
			S+=A
		}
	}

	if K >= S {
		fmt.Println(K)
	} else {
		fmt.Println(S)
	}

}

