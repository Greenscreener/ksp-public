package main

import (
	"fmt"
	   )

func main () {
	var D int
	var S int
	var N int
	
	fmt.Scanf("%d %d %d", &D, &S, &N)
	
	P := 0

	for i := 0; i < N; i++ {
		var A int
		var B int

		fmt.Scanf("%d %d", &A, &B)

		if S >= A && S <= B {
			S+=10
			P++
		}

		S+=D
	}

	fmt.Printf("%d\n", P)
}

