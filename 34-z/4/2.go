package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

const modulator = 1e9 + 7

func main() {
	in := bufio.NewReader(os.Stdin)

	var T int

	fmt.Fscanf(in, "%d\n", &T)
	fmt.Fprintf(os.Stderr, "T: %d\n", T)

	for t := 0; t < T; t++ {
		var N int

		fmt.Fscanf(in, "%d\n", &N)
		fmt.Fprintf(os.Stderr, "N: %d\n", N)

		arrs := make([][]int, 2)
		for i := 0; i < 2; i++ {
			arrs[i] = make([]int, N)
			for n := 0; n < N; n++ {
				fmt.Fscanf(in, "%d", &arrs[i][n])
			}
			sort.Ints(arrs[i])
			fmt.Fscanf(in, "\n")
		}
		destinations := arrs[0]
		planes := arrs[1]
		factors := make([]int, N)

		product := 1
		for n := 0; n < N; n++ {
			factor := sort.Search(N-n, func(i int) bool { return planes[n] < destinations[i+n] })
			product = (product % modulator) * (factor % modulator)
			factors[n] = factor
		}
		fmt.Fprintf(os.Stderr, "%v\n%v\n%v\n", destinations, planes, factors)

		fmt.Println(product)
	}
}
