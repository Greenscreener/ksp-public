package main

import (
	"bufio"
	"fmt"
	"os"
)

func mod(a, b int) int {
	return ((a % b) + b) % b
}

type state struct {
	x, y, o int
}

var ors = map[rune]int{
	'A': 0,
	'>': 1,
	'V': 2,
	'<': 3,
}

func main() {
	in := bufio.NewReader(os.Stdin)
	err := bufio.NewWriter(os.Stderr)

	var N, M, K int

	fmt.Fscanf(in, "%d %d %d\n", &N, &M, &K)

	fmt.Fprintf(err, "%d %d %d\n", N, M, K)

	field := make([][]bool, N)
	endState := make([][]bool, N)
	startStates := make([]state, 0)
	for n := 0; n < N; n++ {
		field[n] = make([]bool, M)
		endState[n] = make([]bool, M)
		for m := 0; m < M; m++ {
			var c rune
			fmt.Fscanf(in, "%c", &c)
			field[n][m] = (c == '#')
			if c == '.' || c == '#' {
				continue
			}
			startStates = append(startStates, state{m, n, ors[c]})
		}
		fmt.Fscanf(in, "\n")
	}

	stateSpace := make(map[state]state)
	loopLength := make(map[state]*int)

	for _, s := range startStates {
		skipped := false
		fmt.Fprintf(err, "Starting with state %v\n", s)
		// fmt.Fprintf(os.Stderr, "\r%d/%d", bruh, len(startStates))
		current := make(map[state]bool)
		for i := 0; i < K; i++ {
			next, known := stateSpace[s]
			lengthPtr, lengthKnown := loopLength[s]
			if !known || skipped || (!lengthKnown && !current[s]) { // If we haven't seen this state or have already skipped on this guard or this state is not member of a loop, figure out the next state manually
				nextX := s.x
				nextY := s.y
				nextState := s
				if s.o == 0 { // Figure out the next spot we should land on
					nextY--
				} else if s.o == 1 {
					nextX++
				} else if s.o == 2 {
					nextY++
				} else if s.o == 3 {
					nextX--
				}
				if nextX < 0 || nextY < 0 || nextX >= M || nextY >= N || field[nextY][nextX] { // If that spot is out of bounds or there's a wall there, rotate, otherwise use that state
					nextState.o++
					nextState.o = mod(nextState.o, 4)
				} else {
					nextState.x = nextX
					nextState.y = nextY
				}
				stateSpace[s] = nextState
				current[s] = true
				s = nextState
				fmt.Fprintf(err, "Generated state: %v i=%d\n", s, i)
			} else { // We found a state that is a member of a loop
				i--
				if lengthKnown { // We know the length of this loop, skip its length as many times as it fits
					skipped = true
					diff := ((K - i) / (*lengthPtr)) * (*lengthPtr)
					fmt.Fprintf(err, "Skipping ((%d - %d) / (%d)) * (%d) = %d steps.\n", K, i, *lengthPtr, *lengthPtr, diff)
					i += diff
				} else { // We don't know this loop yet, measure its length.
					length := 1
					loopLength[s] = &length
					fmt.Fprintf(err, "Looping from %v to %v\n", next, s)
					for next != s { // Go through the same loop until you find the same state again, but note how many steps it takes
						loopLength[next] = &length // Attach a pointer to this length to all members of the loop
						next = stateSpace[next]
						fmt.Fprintf(err, "next: %v\n", next)
						length++
					}
				}
			}
		}
		endState[s.y][s.x] = true
		fmt.Fprintf(err, "\rEnding with state %v\n", s)
	}

	for n := 0; n < N; n++ {
		for m := 0; m < M; m++ {
			c := '.'
			if field[n][m] {
				c = '#'
			} else if endState[n][m] {
				c = 'X'
			}
			fmt.Printf("%c", c)
		}
		fmt.Printf("\n")
	}

}
