package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

func main() {
	in := bufio.NewReader(os.Stdin)

	var N, P, O int

	fmt.Fscanf(in, "%d %d %d\n", &N, &P, &O)

	sadCount := 0

	for i := 0; i < N; i++ {
		var A, B int
		fmt.Fscanf(in, "%d %d\n", &A, &B)
		nearest := int(math.Ceil(float64(A-O)/float64(P)))*P + O
		if B < nearest {
			sadCount++
		}
	}

	fmt.Printf("%d\n", sadCount)
}
