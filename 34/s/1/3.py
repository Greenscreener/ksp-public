from manim import *
import random

def moveArrows(arrows, squares, minN, maxN, current):
	return [				
		arrows[0].animate.next_to(squares[minN],DOWN), 
		arrows[2].animate.next_to(squares[maxN],DOWN),
		arrows[1].animate.next_to(squares[current],DOWN)
	]


class BinarySearch(Scene):
	def construct(self):
		nNumbers = 20
		
		data = [0]*nNumbers
		for i in range(1,nNumbers):
			data[i] = data[i-1]+random.randrange(5)

		squares = [	
			Rectangle(
				width=8/nNumbers,
				height=8/nNumbers, 
				color=WHITE, 
				fill_opacity=0
			)
			for i in range(nNumbers)
		]

		self.play(*[Write(s) for s in squares])

		self.play(VGroup(*squares).animate.arrange())

		numbers = [
			Tex(f"${data[i]}$")
				.set_height(4/nNumbers)
				.move_to(squares[i])
			for i in range(nNumbers)
		]

		self.play(*[Write(n) for n in numbers])

		target = data[random.randrange(nNumbers)]

		text = Tex(f"Hledáme ", f"${target}$")

		self.play(Write(text), VGroup(*squares, *numbers).animate.shift(DOWN/2), text.animate.shift(UP/2))

		self.play(text.animate.set_color_by_tex(f"{target}", GREEN))
		
		minN = 0
		maxN = nNumbers-1
		current = (maxN+minN)//2
		
		arrows = [
			Arrow(
				start=8/nNumbers*DOWN,
				end=0*DOWN,
				color=WHITE
				 )
			for _ in range(3)
		]
		
		self.play(*moveArrows(arrows, squares, minN, maxN, current))
		#self.play(*[Write(i) for i in arrows])

		while data[current] != target:
			animations = []
			if data[current] < target:
				for i in range(minN, current+1):
					animations.append(numbers[i].animate.set_color(GRAY))
					animations.append(squares[i].animate.set_color(GRAY))
				minN = current+1
			elif data[current] > target:
				for i in range(current, maxN+1):
					animations.append(numbers[i].animate.set_color(GRAY))
					animations.append(squares[i].animate.set_color(GRAY))
				maxN = current-1

			current = (maxN+minN)//2

			self.play(*moveArrows(arrows, squares, minN, maxN, current), *animations)

		animations = []
		for i in range(minN, maxN+1):
			animations.append(numbers[i].animate.set_color(GRAY))
			animations.append(squares[i].animate.set_color(GRAY))

		minN = current
		maxN = current
		animations.append(numbers[current].animate.set_color(GREEN))
		animations.append(squares[current].animate.set_color(GREEN))
		self.play(*moveArrows(arrows, squares, minN, maxN, current), *animations)

		self.wait(2)
