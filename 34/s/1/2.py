from manim import *
import random

class BubbleSort(Scene):
	def construct(self):
		nColumns = 20
		resolution = 100
		maxHeight = 3

		data = [random.randrange(resolution) for _ in range(nColumns)]

		columns = [	
			Rectangle(
				width=8/nColumns,
				height=(data[i]/resolution*maxHeight), 
				color=WHITE, 
				fill_opacity=1
			)
			for i in range(nColumns)
		]

		VGroup(*columns).arrange()
		for column in columns:
			column.align_to([0,-3],DOWN)


		self.play(*[Write(c) for c in columns])

		for i in range(nColumns):
			for j in range(nColumns-1-i):
				duration = 0.5/(i+1)
				self.play(
					columns[j].animate.set_color(GREEN),
					columns[j+1].animate.set_color(GREEN),
					run_time=duration
				)
				if (data[j] > data[j+1]):
					a = data[j]
					data[j] = data[j+1]
					data[j+1]=a
					arrow = CurvedDoubleArrow(start_point=columns[j].get_bottom(), end_point=columns[j+1].get_bottom())
					if i == 0:
						self.play(Write(arrow), run_time=.2)
					self.play(
						columns[j].animate.stretch_to_fit_height(data[j]/resolution*maxHeight).align_to([0,-3],DOWN),
						columns[j+1].animate.stretch_to_fit_height(data[j+1]/resolution*maxHeight).align_to([0,-3],DOWN),
						run_time=duration

					)
					if i == 0:
						self.play(Uncreate(arrow), run_time=.1)

				self.play(
					columns[j].animate.set_color(WHITE),
					columns[j+1].animate.set_color(WHITE),
					run_time=duration

				)
