from manim import *


class Nice(Scene):
	def construct(self):
		t1 = Tex("Heck")
		t2 = Tex("yes!")

		line1 = VGroup(t1,t2)

		self.play(Write(t1))
		self.play(Write(t2), line1.animate.arrange())

		self.play(t2.animate.set_color(RED))

		circle = Circle(color=GREEN).shift(DOWN)

		self.play(line1.animate.shift(UP), Write(circle))

		t3 = Tex("$\\frac{3+4i}{2-5i}$")
		t3.move_to(circle)

		self.play(Write(t3))

		self.wait(2)
