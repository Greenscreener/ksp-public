from manim import *
import random


class CirclesShuffle(Scene):
	def construct(self):
		nCircles = 5
		radius = 4/nCircles
		nShuffles = 13

		circles = [Circle(radius=radius, color=WHITE, fill_opacity=1) for _ in range(nCircles)]

		VGroup(*circles).arrange()

		self.play(*[Write(c) for c in circles])

		self.play(circles[0].animate.set_color(RED))
		self.play(circles[0].animate.set_color(WHITE))

		for _ in range(nShuffles):
			self.play(Swap(circles[random.randrange(nCircles)], circles[random.randrange(nCircles)]), run_time=10/nShuffles)

		self.play(circles[0].animate.set_color(RED))
#		self.play(circles[0].animate.shift(UP*1))
#		self.play(circles[0].animate.shift(DOWN*1))
		self.play(circles[0].animate.set_color(WHITE))

