from manim import *

class FibSquare(VGroup):
	def __init__(self, side=1, **kwargs):
		super().__init__(**kwargs)
		square = Rectangle(width=side, height=side)
		number = Tex(f'${side}^2$', height=side*0.4)
		number.move_to(square)
		self.add(square, number)

def dir(n):
	if n % 4 == 0:
		return DOWN
	elif n % 4 == 1:
		return RIGHT
	elif n % 4 == 2:
		return UP
	elif n % 4 == 3:
		return LEFT

class Fib(MovingCameraScene):
	def construct(self):
		iterations = 12
		fib = [1]
		s = FibSquare(1)
		all = VGroup(s)
		squares = [s]
		self.camera.frame.set(height=1.5)
		self.play(Write(s))
		def update_camera(camera):
			camera.move_to(all.get_center())
			camera.set(height=all.height*1.5)
		self.camera.frame.add_updater(update_camera)
		self.add(self.camera.frame)
		for n in range(1,iterations):
			if n < 2:
				fib.append(1)
			else:
				fib.append(fib[n-1]+fib[n-2])
			s = FibSquare(fib[n]).next_to(all, dir(n), buff=0)
			all.add(s)
			self.play(FadeIn(s, shift=fib[n-1]*dir(n), scale=0))
			squares.append(s)

		self.camera.frame.clear_updaters()

		dot = Dot(point=squares[0].get_center()+(UP+LEFT)*0.5)
		self.play(Write(dot))
		self.play(self.camera.frame.animate.set(height=2).move_to(dot), all.animate.set_color(GRAY_D))
		path = TracedPath(dot.get_center)
		self.add(path)
		def update_camera(camera):
			camera.move_to(dot.get_center())
			camera.set(height=max(path.height,2))
		self.camera.frame.add_updater(update_camera)
		for n in range(0,iterations):
			self.play(Rotate(dot, angle=PI/2, rate_func=linear, about_point=squares[n].get_center()+(dir(n+1)+dir(n+2))*fib[n]*0.5))

		self.camera.frame.clear_updaters()
		path.clear_updaters()
		self.play(self.camera.frame.animate.move_to(all).set(height=all.height*1.5), path.animate.set(stroke_width=10))

		self.wait(3)

