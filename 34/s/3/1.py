from manim import *
import random
from networkx.generators.random_graphs import gnp_random_graph
from networkx import is_connected
from collections import deque

def colorEdge(a,b,g,color):
	try:
		return g.edges[(a,b)].animate.set_color(color)
	except KeyError:
		return g.edges[(b,a)].animate.set_color(color)

class DFS(Scene):
	def construct(self):
		nVerts = 25
		random.seed(0xDEADBEEFBADF00D+5) # Pokus o nějaký alespoň trochu hezky vypadající seed
		graph = None
		while graph is None or not is_connected(graph):
			graph = gnp_random_graph(nVerts, 3/nVerts)
		neighbours = [set() for _ in range(nVerts)]
		for edge in graph.edges:
			neighbours[edge[0]].add(edge[1])
			neighbours[edge[1]].add(edge[0])
		g = Graph(graph.nodes, graph.edges, layout_config={"seed": 0}).scale(2.2)
		self.play(Write(g))
		print(neighbours)
		# Body 3, 12, 20 by mohly být dobré počáteční body
		path = deque()
		visited=[False]*nVerts
		path.append(3)
		last = None
		waitingAnimation = []
		popping = False
		spanningTree = []
		while len(path) > 0:
			v = path[-1]
			visited[v]=True
			if last != None:
				waitingAnimation.append(
						AnimationGroup(
							ReplacementTransform(
								Dot(g.vertices[last].get_center()).scale(2).set_color(RED),
								Dot(g.vertices[v].get_center()).scale(2).set_color(RED)
								),
							colorEdge(last,v,g,RED),
							lag_ratio=.4)
						)
			last = v
			moveAndFlash = [
					AnimationGroup(
						*waitingAnimation,
						lag_ratio=0),
					]
			if not popping:
				moveAndFlash.append(
						AnimationGroup(
							Flash(g.vertices[v], color=RED),
							g.vertices[v].animate.set_color(RED),
							lag_ratio=0),
						)
			self.play(
					AnimationGroup(
						*moveAndFlash,
						lag_ratio=0.24)
					)
			unvisitedNeighbours = list(filter(lambda n: not visited[n], neighbours[v]))
			if len(unvisitedNeighbours) > 0:
				self.play(*[
						g.vertices[n].animate.set_color(BLUE)
						for n in unvisitedNeighbours
					], *[
						colorEdge(v,n,g,BLUE)
						for n in unvisitedNeighbours
					])
				waitingAnimation = [
						*[
							g.vertices[n].animate.set_color(WHITE)
							for n in unvisitedNeighbours[1:]
						], *[
							colorEdge(v,n,g,WHITE)
							for n in unvisitedNeighbours[1:]
						],
						g.vertices[v].animate.set_color(RED) # Jinak se hrany zobrazují přes bod
					]
				path.append(unvisitedNeighbours[0])
				spanningTree.append((v,unvisitedNeighbours[0]))
				popping = False
			else:
				popping = True
				path.pop()
		
		self.clear()
		self.add(g)
		g.remove_edges(*graph.edges)
		g.add_edges(*spanningTree)
		self.play(g.animate.change_layout()) # Vypadá jako škorpion 😁
		self.wait(5)
