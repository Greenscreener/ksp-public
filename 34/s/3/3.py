from manim import *
import numpy as np
import time

inst = "LLRR"


def dir(n):
	if n % 4 == 0:
		return [0,-1,0]
	elif n % 4 == 1:
		return [1,0,0]
	elif n % 4 == 2:
		return [0,1,0]
	elif n % 4 == 3:
		return [-1,0,0]

def action(n):
	return -1 if inst[n % len(inst)] == "L" else 1

def aColor(n):
	if n % len(inst) == 0:
		return BLACK
	elif n % len(inst) == 1:
		return RED
	elif n % len(inst) == 2:
		return GREEN
	elif n % len(inst) == 3:
		return YELLOW


# while True:
	# r+=action(map[p[1]][p[0]])
	# op = p.copy()
	# p+=dir(r)
	# map[op[1]][op[0]]+=1
	# print(it)
	# it+=1
	# print("\n".join(["".join(["." if n % len(inst) == 0 else "#" for n in row]) for row in map]))
	# print("\033[F"*(len(map)+2))
# 

class Langton(MovingCameraScene):
	def construct(self):
		map = [[0 for _ in range(0,11)] for _ in range(0,11)]
		center = np.array([len(map[0])//2,len(map)//2,0])
		p = np.copy(center)
		r = 1
		it = 0
		squares = []
		for y in range(len(map)):
			for x in range(len(map[0])):
				squares.append(Square(stroke_color=WHITE, fill_color=BLACK, fill_opacity=1, stroke_width=3).scale(.5))

		squares = VGroup(*squares).arrange_in_grid(rows=len(map), buff=0)
		self.play(Write(squares))

		ant = SVGMobject("ant.svg").scale(.25).move_to(p-center)
		self.play(Write(ant))
		self.play(Rotate(ant, -PI/2))

		def update_camera(camera):
			camera.move_to(ant.get_center())
		self.camera.frame.add_updater(update_camera)
		self.add(self.camera.frame)

		for i in range(0,200): # Sem bych dal alespoň tak 10k, ale je to hrozně pomalý, když jsem to pouštěl bez manimu jenom do terminálu, tak to dělalo fakt pěkný obrazce.
			rt = .5 if i < 10 else 0.06
			a=action(map[p[1]][p[0]])
			r+=a
			self.play(Rotate(ant, -a*PI/2), run_time=rt)
			map[p[1]][p[0]]+=1
			op=p.copy()
			p+=dir(r)
			self.play(
					ant.animate.move_to((p-center)*[1,-1,0]),
					squares[op[1]*len(map)+op[0]].animate.set_fill(aColor(map[op[1]][op[0]])),
					run_time=rt)
			
		self.camera.frame.clear_updaters()
		self.play(self.camera.frame.animate.move_to([0,0,0]).scale(1.5))


