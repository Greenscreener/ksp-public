# from manim import *
import numpy as np
import time

inst = "RLLR"


def dir(n):
	if n % 4 == 0:
		return [0,-1]
	elif n % 4 == 1:
		return [1,0]
	elif n % 4 == 2:
		return [0,1]
	elif n % 4 == 3:
		return [-1,0]

def action(n):
	return -1 if inst[n % len(inst)] == "L" else 1

map = [[0 for _ in range(0,100)] for _ in range(0,100)]
p = np.array([50,50])
r = 1
it = 0

while True:
	r+=action(map[p[1]][p[0]])
	op = p.copy()
	p+=dir(r)
	map[op[1]][op[0]]+=1
	print(it)
	it+=1
	print("\n".join(["".join(["." if n % len(inst) == 0 else "#" for n in row]) for row in map]))
	print("\033[F"*(len(map)+2))
