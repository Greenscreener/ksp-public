from manim import *
import math

class Path(VMobject):
	def __init__(self, points, *args, **kwargs):
		super().__init__(self, *args, **kwargs)
		self.set_points_as_corners(points)

	def get_important_points(self):
		"""Vrátí důležité body křivky."""
		# drobné vysvětlení: Manim k vytváření úseček používá kvadratické Bézierovy křivky
		# - každá taková křivka má čtyři body -- dva krajní a dva řidicí
		# - path.points vrací *všechny* body, což po několika iteracích roste exponenciálně
		#
		# proto používáme funkce get_*_anchors, které vrací pouze krajní body
		# pro více detailů viz https://en.wikipedia.org/wiki/Bézier_curve
		return list(self.get_start_anchors()) + [self.get_end_anchors()[-1]]

class Hilbert(Scene):
	def construct(self):
		scale = 2
		path = Path([LEFT+DOWN, LEFT+UP, RIGHT+UP, RIGHT+DOWN]).scale(scale)
		topEdges = 1
		self.play(Write(path))
		for i in range(6):
			
			topEdges = topEdges*2+1
			self.play(path.animate.scale(1/2-1/(topEdges*2),about_edge=LEFT+UP).set_color(GRAY))
				
			path2 = path.copy()

			self.play(path2.animate.shift(RIGHT*(path.width+4/topEdges)))

			path3 = path.copy()
			path4= path2.copy()

			self.play(path3.animate.shift(DOWN*(path.width+4/topEdges)).rotate(-math.pi/2).flip(LEFT), path4.animate.shift(DOWN*(path.width+4/topEdges)).rotate(math.pi/2).flip(LEFT))

			oldpath = path
			path = Path([*path3.get_important_points(), *path.get_important_points(), *path2.get_important_points(), *path4.get_important_points()])
			self.play(Write(path), run_time=2)
			self.remove(oldpath, path2, path3, path4)

		


