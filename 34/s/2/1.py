from manim import *
import math
import random

def epii(angle):
	return RIGHT*math.cos(angle)+UP*math.sin(angle)

def polygonPoint(n, i, r):
	return r*epii(1/2*math.pi+i*2*math.pi/n)

class Polygon(Scene):
	def construct(self):

		n = 3
		r = 2
		
		points = VGroup(*[
			Dot().shift(polygonPoint(n, i, r))
			for i in range(n)
		])

		self.play(Write(points))

		labels = VGroup(*[
			Tex(chr(ord("z")-i))
			for i in range(n)
		])

		def pointUpdaterator(i):
			def pointUpdater(object):
				object.next_to(points[i], polygonPoint(n, i, 1/2))

			return pointUpdater


		for i in range(n):
			labels[i].add_updater(pointUpdaterator(i))

		self.play(Write(labels))

		circle = Circle()	

		def circleUpdate(object):
			object.become(Circle.from_three_points(*[point.get_center() for point in points[:3]], color=RED))

		circle.add_updater(circleUpdate)

		self.play(Write(circle))

		lines = VGroup(*[Line() for _ in range(n)])

		def lineUpdaterator(i):
			def lineUpdater(object):
				linePoints = []
				if i == 0:
					linePoints = [ points[n-1], points[i] ]
				else: 
					linePoints = [ points[i-1], points[i] ]
				object.become(Line(*[point.get_center() for point in linePoints]))

			return lineUpdater

		for i in range(n):
			lines[i].add_updater(lineUpdaterator(i))

		self.play(Write(lines))

		for _ in range(4):
			self.play(points.animate.rotate(math.pi*1/2), run_time=1)

		# Tady tady už se kolečko s více-než-trojúhélníkama robije :(
		for i in range(n):
			self.play(points[i].animate.shift(1.5*epii(random.random()*2*math.pi)))

