package main

import (
	"bufio"
	"fmt"
	"math"
	"os"

	pq "github.com/jupp0r/go-priority-queue"
)

func main() {
	//f, _ := os.Open("1.in")
	f := os.Stdin
	in := bufio.NewReader(f)

	var N, M, J, S int

	fmt.Fscanf(in, "%d %d %d %d\n", &N, &M, &J, &S)

	vertices := make([][][]int, N)
	for i := range vertices {
		vertices[i] = make([][]int, 0)
	}
	costs := make([][]int, 3)
	for i := range costs {
		costs[i] = make([]int, N)
		for j := range costs[i] {
			costs[i][j] = -1
		}
	}

	for i := 0; i < M; i++ {
		var a, b, d int

		fmt.Fscanf(in, "%d %d %d\n", &a, &b, &d)

		vertices[a-1] = append(vertices[a-1], []int{b - 1, d})
		vertices[b-1] = append(vertices[b-1], []int{a - 1, d})
	}

	for s := 0; s < 3; s++ {
		q := pq.New()
		costs[s][s] = 0
		q.Insert(s, math.Inf(1))
		for q.Len() != 0 {
			temp, _ := q.Pop()
			v := temp.(int)
			for i := range vertices[v] {
				v2 := vertices[v][i][0]
				v2d := vertices[v][i][1]
				v2fd := costs[s][v] + v2d
				if costs[s][v2] == -1 || costs[s][v2] > v2fd {
					costs[s][v2] = v2fd
					q.Insert(v2, float64(v2fd))         // If we insert an element that already exists, this will not do anything, so we'll update its priority accordingly
					q.UpdatePriority(v2, float64(v2fd)) // If we just inserted the element, this should do nothing
				}
			}
		}
	}

	minCost := math.MaxInt
	//meetingPoint := 0
	for i := range costs[0] {
		cost := costs[0][i]*J + costs[1][i]*J + costs[2][i]*S
		if minCost > cost {
			minCost = cost
			//meetingPoint = i + 1
		}
	}

	//fmt.Fprintf(os.Stderr, "%d\n%v\n", meetingPoint, costs)

	fmt.Println(minCost)
}
