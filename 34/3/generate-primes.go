package main

import (
	"fmt"

	"github.com/kavehmz/prime"
)

func main() {
	var N, K uint64
	fmt.Scanf("%d %d", &N, &K)
	fmt.Println(len(prime.Primes(N)))
	fmt.Println(len(prime.Primes(K)))
}
