package main

import (
	"fmt"
	   )


func main () {
	values := [...]int{1, 2, 5, 10, 20, 50}

	var T int
	
	fmt.Scanf("%d", &T)
	

	for i := 0; i < T; i++ {
		var c int
		fmt.Scanf("%d", &c)

		total := 0

		for j := 0; j < 6; j++ {
			var coin int
			fmt.Scanf("%d", &coin)
			total+=values[j]*coin
		}
		
		if total >= c {
			fmt.Println("ANO")
		} else {
			fmt.Println("NE")
		}

	}

}

