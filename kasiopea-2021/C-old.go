package main

import (
	"fmt"
	"bufio"
	"os"
	"strings"
	   )

func or(arr []bool) bool {
	for i := range arr {
		if arr[i] {
			return true
		}
	}
	return false
}

func main () {
	in := bufio.NewReader(os.Stdin)
	var T int
	
	fmt.Fscanf(in,"%d\n", &T)

	for i := 0; i < T; i++ {
		var R, S, K int
		fmt.Fscanf(in, "%d %d %d\n", &R, &S, &K)
		
		dort := make([][]int, R)
		dropletLocations := make([][]int, K)
		for r := 0; r < R; r++ {
			dort[r] = make([]int, S)
			for s := 0; s < S; s++ {
				var f int
				fmt.Fscanf(in, "%d", &f)
				dort[r][s] = f
				if f != -1 {
					dropletLocations[f-1] = []int{r,s}
				}
			}
			fmt.Fscanf(in, "\n")
		}

		initDort := dort

		for f := range dropletLocations {
			canCrawl := []bool{true,true,true,true} // Left, right, up, down
			r := dropletLocations[f][0]
			s := dropletLocations[f][1]
			bounds := []int{s,s,r,r}
			
		    for or(canCrawl) {
				if canCrawl[0] {
					if bounds[0] <= 0 {
						canCrawl[0] = false
					} else {
						for n := bounds[2]; n <= bounds[3]; n++ {
							if dort[n][bounds[0]-1] != -1 {
								canCrawl[0] = false
							}
						}
						if canCrawl[0] {
							bounds[0]--
						}
					}
				}
				if canCrawl[1] {
					if bounds[1] >= S-1 {
						canCrawl[1] = false
					} else {
						for n := bounds[2]; n <= bounds[3]; n++ {
							if dort[n][bounds[1]+1] != -1 {
								canCrawl[1] = false
							}
						}
						if canCrawl[1] {
							bounds[1]++
						}
					}
				}
				if canCrawl[2] {
					if bounds[2] <= 0 {
						canCrawl[2] = false
					} else {
						for n := bounds[0]; n <= bounds[1]; n++ {
							if dort[bounds[2]-1][n] != -1 {
								canCrawl[2] = false
							}
						}
						if canCrawl[2] {
							bounds[2]--
						}
					}
				}
				if canCrawl[3] {
					if bounds[3] >= R-1 {
						canCrawl[3] = false
					} else {
						for n := bounds[0]; n <= bounds[1]; n++ {
							if dort[bounds[3]+1][n] != -1 {
								canCrawl[3] = false
							}
						}
						if canCrawl[3] {
							bounds[3]++
						}
					}
				}
			}
			
			for r := bounds[2]; r <= bounds[3]; r++ {
				for s := bounds[0]; s <= bounds[1]; s++ {
					dort[r][s] = f+1
				}
			}

		}	
		complete := true
		for r := 0; r < R && complete; r++ {
			for s := 0; s < S && complete; s++ {
				if dort[r][s] == -1 {
					complete = false
				}
			}
		}
	    if !complete {
			dort = initDort
	    }
	

		out := make([]string, R)
		for r := 0; r < R; r++ {
			out[r] = strings.ReplaceAll(strings.ReplaceAll(fmt.Sprint(dort[r]), "[", ""), "]", "")
		}
		fmt.Println(strings.Join(out,"\n"))
		

	}

}

