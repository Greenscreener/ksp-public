package main

import (
	"fmt"
	"bufio"
	"os"
	   )

type queue struct {
        start *indice
        end *indice
}

type indice struct {
        value int
        next *indice
}

func newQueue() queue {
        return queue{nil,nil}
}

func (q *queue) enqueue(value int) {
        i := indice{value, nil}
        if q.start == nil {
                q.start = &i
                q.end = &i
        } else {
                e := q.end
                q.end = &i
                e.next = &i
        }
}

func (q *queue) dequeue() int {
        s := q.start
        q.start = s.next
        return s.value
}

func (q *queue) isEmpty() bool {
        if (q.start == nil) {
                return true
        } else {
                return false
        }
}




func main () {
	in := bufio.NewReader(os.Stdin)
	var T int
	
	fmt.Fscanf(in,"%d\n", &T)

	for i := 0; i < T; i++ {
		var R, S, K int
		fmt.Fscanf(in, "%d %d %d\n", &R, &S, &K)
		
		walls := make([]bool, R*S)
		distances := make([]int, R*S)
		for j := range distances {
			distances[j] = int(^uint(0) >> 1) 
		}
		door := -1

		for r := 0; r < R; r++ {
			for s := 0; s < S; s++ {
				var c rune
				fmt.Fscanf(in, "%c", &c)
				if c == 'D' {
					if door != -1 {
						panic("two doors?? how????")
					}
					door = S*r+s
				}
				if c == 'X' {
					walls[S*r+s] = true
				} else {
					walls[S*r+s] = false
				}

			}
			fmt.Fscanf(in, "\n")
		}

		q := newQueue()
		q.enqueue(door)
		distances[door] = 0
		possible := true
		totalDistance := 0
		// fmt.Printf("Loaded map: %v\nBeginning inspection.\n", walls)
		for k := 0; k < K; k++ {
			if q.isEmpty() {
				possible = false
				break
			}
			current := q.dequeue()
			totalDistance+=2*distances[current]
			// fmt.Printf("Current index: %d-%d\n",current%S, int(current/S))
			for direction,next := range []int{current-1, current+1, current-S, current+S} {
				if next < 0 || 
					next >= R*S || 
					walls[next] == true ||
					distances[next] <= distances[current]+1 ||
					(current%S == 0 && direction == 0) || 
					(current%S == S-1 && direction == 1) {
						continue
				}
				q.enqueue(next)
				distances[next] = distances[current]+1
			}
		}

	    if possible {
			fmt.Printf("%d\n", totalDistance)
	    } else {
	 	    fmt.Println("-1")
	    }
		

	}

}

