package main

import (
	"fmt"
	"bufio"
	"os"
	"strings"
	"sort"
	"github.com/fogleman/gg"
	"math/rand"
	   )

type queue struct {
        start *indice
        end *indice
}

type indice struct {
        value job
        next *indice
}

func newQueue() queue {
        return queue{nil,nil}
}

func (q *queue) enqueue(value job) {
        i := indice{value, nil}
        if q.start == nil {
                q.start = &i
                q.end = &i
        } else {
                e := q.end
                q.end = &i
                e.next = &i
        }
}

func (q *queue) dequeue() job {
        s := q.start
        q.start = s.next
        return s.value
}

func (q *queue) isEmpty() bool {
        if (q.start == nil) {
                return true
        } else {
                return false
        }
}



type job struct {
	points [][]int
	bounds [][]int
	dir bool 
}

func divideArea(j job, dort [][]int) ([]job, [][]int) {
	points := j.points
	bounds := j.bounds
	dir := j.dir
	println(fmt.Sprintf("Bounds: %v", bounds))
	println(fmt.Sprintf("Points: %v", len(points)))
	if len(points) == 0 {
		return []job{}, dort
	}
	if len(points) == 1 {
		for r := bounds[0][0]; r <= bounds[0][1]; r++ {
			for s := bounds[1][0]; s <= bounds[1][1]; s++ {
				dort[r][s] = points[0][2]
			}
		}
		return []job{}, dort
	}
	dirIndex := 0
	if !dir {
		dirIndex = 1
	}	
	divisionValues := make([]int, len(points))
	for i := range points {
		divisionValues[i] = points[i][dirIndex]
	}
	sort.Ints(divisionValues)
	median := divisionValues[len(divisionValues)/2]
	newPoints0 := make([][]int, 0)
	newPoints1 := make([][]int, 0)
	for i := range points {
		if (points[i][dirIndex] < median) {
			newPoints0 = append(newPoints0,points[i])
		} else {
			newPoints1 = append(newPoints1,points[i])
		}
	}
	newBounds0 := bounds
	newBounds0[dirIndex][1] = median-1
	newBounds1 := bounds
	newBounds1[dirIndex][0] = median
	if len(newPoints0) == 0 {
		newNewPoints1 := make([][]int, 0)
		for i := range newPoints1 {
			if (newPoints1[i][dirIndex] > median) {
				newPoints0 = append(newPoints0,newPoints1[i])
			} else {
				newNewPoints1 = append(newNewPoints1, newPoints1[i])
			}
		}
		newPoints1 = newNewPoints1
		newBounds0[dirIndex][0] = median+1 
		newBounds0[dirIndex][1] = bounds[dirIndex][1]
		newBounds1[dirIndex][0] = median
		newBounds1[dirIndex][1] = median
	}
	return []job{job{newPoints0, newBounds0, !dir},job{newPoints1, newBounds1, !dir}}, dort
}

func main () {
	in := bufio.NewReader(os.Stdin)
	var T int
	
	fmt.Fscanf(in,"%d\n", &T)

	for i := 0; i < 2; i++ {
		var R, S, K int
		fmt.Fscanf(in, "%d %d %d\n", &R, &S, &K)
		
		dort := make([][]int, R)
		dropletLocations := make([][]int, K)
		for r := 0; r < R; r++ {
			dort[r] = make([]int, S)
			for s := 0; s < S; s++ {
				var f int
				fmt.Fscanf(in, "%d", &f)
				dort[r][s] = f
				if f != -1 {
					dropletLocations[f-1] = []int{r,s,f}
				}
			}
			fmt.Fscanf(in, "\n")
		}

		//initDort := dort

		q := newQueue()
		
		dc := gg.NewContext(S, R)
		dc.SetRGB(0, 0, 0)
		for i := range dropletLocations {
			dc.SetPixel(dropletLocations[i][0], dropletLocations[i][1])
		}
		dc.SetRGB(1, 0, 0)
		q.enqueue(job{dropletLocations, [][]int{[]int{0, R-1},[]int{0, S-1}}, false})
		for !q.isEmpty() {
			j := q.dequeue()
			var newJ []job
			newJ, dort = divideArea(j, dort)
			for i := range newJ {
				q.enqueue(newJ[i])
				dc.DrawRectangle(float64(newJ[i].bounds[0][0]),float64(newJ[i].bounds[1][0]),float64(newJ[i].bounds[0][1]),float64(newJ[i].bounds[1][1]))
				dc.Fill()
				dc.SetRGB(rand.Float64(), rand.Float64(), rand.Float64())
				for j := range newJ[i].points {
					dc.SetPixel(newJ[i].points[j][1], newJ[i].points[j][0])
				}
			}
		}
		dc.SavePNG("/tmp/wat.png")

	

		complete := true
		for r := 0; r < R && complete; r++ {
			for s := 0; s < S && complete; s++ {
				if dort[r][s] == -1 {
					complete = false
				}
			}
		}


	    //if !complete && false {
		//	dort = initDort
		//	fmt.Println("nope")
		//	continue
	    //}
	

		out := make([]string, R)
		for r := 0; r < R; r++ {
			out[r] = strings.ReplaceAll(strings.ReplaceAll(fmt.Sprint(dort[r]), "[", ""), "]", "")
		}
		fmt.Println(strings.Join(out,"\n"))
		

	}

}

