package main

import (
	"fmt"
	"bufio"
	"os"
)

const MaxInt = int(^uint(0) >> 1)

func max(a int, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}
func min(a int, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func main() {
	in := bufio.NewReader(os.Stdin)

	var T int

	fmt.Fscanf(in, "%d\n", &T)

	for i := 0; i < T; i++ {
		var N int
		fmt.Fscanf(in, "%d\n", &N)

		nums := make([][]int, 3)

		for y := 0; y < 3; y++ {
			nums[y] = make([]int, N)
			for x := 0; x < N; x++ {
				fmt.Fscanf(in, "%1d", &nums[y][x])
			}
			fmt.Fscanf(in, "\n")
		}
		costs := make([]int,2)
		//historicalCosts := make([][]int,N)
		costs[1] = MaxInt
		costs[0] = 0
		for x := N - 1; x >= 0; x-- {
			newCosts := make([]int,2)

		  	for n := 0; n < 2; n++ {
				newCosts[n] = MaxInt
				A := nums[0][x]
				B := nums[1][x]
				C := nums[2][x] + n*10 // If we want to carry, our target is +10
				for o := 0; o < 2; o++ {
					// Is it possible for the previous to carry/not carry?
					if costs[o] != MaxInt {
						diff := C - (A + B + o) // Calculate the difference between target and A+B and the previous carry
						if diff == 19 || diff == -19 {
							newCosts[n] = min(costs[o]+3, newCosts[n])
						} else if diff == 0 {
							newCosts[n] = min(costs[o], newCosts[n]) // If there is no diff, the target is correct and there is no additional cost
						} else if diff > 0 { // Target is too big, we must add to A+B or subtract from C
							if min(A,B)+diff < 10 || // The diff can be all added into one of A or B, only one change is necessary
							   C-n*10 >= diff { // The diff can be all subtracted from C, also only one change is necessary
								newCosts[n] = min(costs[o]+1, newCosts[n]) 
							} else { // We should never need more than 2 changes
								newCosts[n] = min(costs[o]+2, newCosts[n])
							}
						} else { // Target is too small, we must subtract from A+B or add to C
							if max(A,B) >= -diff || // The diff can be all subtracted from one of A or B, only one change is necessary
							   C-n*10-diff < 10 { // The diff can be all added to C, also only one change is necessary
								newCosts[n] = min(costs[o]+1, newCosts[n])
						    } else { // We should never need more than 2 changes
								newCosts[n] = min(costs[o]+2, newCosts[n])
						    }
						}
					}
				}
		  	}
			costs = newCosts
			//historicalCosts[x] = newCosts

		}
		fmt.Printf("%d\n",costs[0])

	}

}
