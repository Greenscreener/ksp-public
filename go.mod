module gitlab.com/greenscreener/ksp

go 1.17

require github.com/fogleman/gg v1.3.0

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/jupp0r/go-priority-queue v0.0.0-20160601094913-ab1073853bde // indirect
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
)
